var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require("webpack");
const WebpackDevServerOutput = require('webpack-dev-server-output');

var extractPluginMain = new ExtractTextPlugin({
    filename: "src/css/main.css"
})

var extractPluginPrisijungimas = new ExtractTextPlugin({
    filename: "src/css/prisijungimas.css"
})

var extractPluginValdymas = new ExtractTextPlugin({
    filename: "src/css/valdymas.css"
})

module.exports =
[
    {
        entry: "./src/js/app/root.js",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "src/main.js",
            publicPath: 'http://localhost:8080'
        },
        devServer: {
            inline: true,
            host: "localhost",
            port: 8080,
            contentBase: path.join(__dirname, "dist/src"),
            proxy: {
                "*": "http://localhost:8000"
            }
        },
        module:
        {
            rules:[
                {
                    test: /\.js$/,
                    use:[
                        {
                            loader: 'babel-loader',
                            options:
                            {
                                presets:["es2015"]
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: extractPluginMain.extract({
                        use: ["css-loader", "sass-loader"]
                    })
                },
                {
                    test: /\.php$/,
                    use: [
                        {
                            loader: "file-loader",
                            options:
                            {
                                name: "[path][name].[ext]"
                            }
                        },
                        {
                            loader: "html-minifier-loader"
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: extractPluginMain.extract({
                        use: ["css-loader"]
                    })
                },
                {
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    loader: 'url-loader'
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "file-loader",
                            options:
                            {
                                name: "[path][name].[ext]"
                            }
                        },
                        {
                            loader: "html-minifier-loader"
                        }
                    ]
                }
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                jquery: "jquery"
            }),
            extractPluginMain,
            new WebpackDevServerOutput({
        		path: 'dist',
        		isDel: false
        	})
        ]
    },
    {
        entry: "./src/js/prisijungimas/root.js",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "src/prisijungimas.js"
        },
        module:
        {
            rules:[
                {
                    test: /\.js$/,
                    use:[
                        {
                            loader: 'babel-loader',
                            options:
                            {
                                presets:["es2015"]
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: extractPluginPrisijungimas.extract({
                        use: ["css-loader", "sass-loader"]
                    })
                },
                {
                    test: /\.css$/,
                    use: extractPluginPrisijungimas.extract({
                        use: ["css-loader"]
                    })
                },
                {
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    loader: 'url-loader'
                }
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                jquery: "jquery"
            }),
            extractPluginPrisijungimas
        ]
    },
    {
        entry: "./src/js/valdymas/root.js",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "src/valdymas.js"
        },
        module:
        {
            rules:[
                {
                    test: /\.js$/,
                    use:[
                        {
                            loader: 'babel-loader',
                            options:
                            {
                                presets:["es2015"]
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    use: extractPluginValdymas.extract({
                        use: ["css-loader", "sass-loader"]
                    })
                },
                {
                    test: /\.css$/,
                    use: extractPluginValdymas.extract({
                        use: ["css-loader"]
                    })
                },
                {
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    loader: 'url-loader'
                }
            ]
        },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                jquery: "jquery"
            }),
            extractPluginValdymas
        ]
    }
]
