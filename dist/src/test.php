<?php
    include_once(__DIR__ . "/includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);

    $before = microtime(true);

    bench2();

    $after = microtime(true);
    echo $after-$before;

    function insert()
    {
        for ($i=0 ; $i<10000 ; $i++)
        {
            $lat = rand(-90000, 90000) / 1000;
            $lon = rand(-180000, 18000) / 1000;

            $stmt = $GLOBALS['pdo']->prepare
            ("
                INSERT INTO Test (location)
                VALUES (GeomFromText('POINT(".$lat." ".$lon.")'))
            ");

            $stmt->execute();
        }
    }

    function bench1()
    {
        for ($i=0 ; $i<1000 ; $i++)
        {
            $stmt = $GLOBALS['pdo']->prepare
            ("
                SELECT
                    X(locationSimple) AS 'latitude',
                    Y(locationSimple) AS 'longitude',
                    (
                        GLength(
                            LineStringFromWKB(
                                LineString(
                                locationSimple,
                                GeomFromText('POINT(51.5177 -0.0968)')
                            )
                        )
                    )
                ) AS distance
                FROM ItemsGeneral
                WHERE X(locationSimple) < 0
                ORDER BY distance ASC
            ");

            $stmt->execute();
        }
    }

    function bench2()
    {
        for ($i=0 ; $i<1000 ; $i++)
        {
            $stmt = $GLOBALS['pdo']->prepare
            ("
                SELECT
                    X(location) AS 'latitude',
                    Y(location) AS 'longitude'
                FROM Test
                WHERE
                    (X(location) BETWEEN -3 AND 3) AND
                    (Y(location) BETWEEN -95 AND -92)
            ");

            $stmt->execute();
        }
    }
?>