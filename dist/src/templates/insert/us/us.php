<div class="container"><div class="row"><div class="col-sm-12"><div class="topicSelectionBtn">tests->asdfa</div><form id="insertForm"><h3>Bendra info</h3><div id="generalInsertForm"><label>Antraštė: <input placeholder="Antraštė" type="text" class="form-control"></label><label class="displayBlock">Aprašymas<textarea class="form-control" placeholder="Aprašymas"></textarea></label><div id="insertLocationMap"></div><div id="tempThumbnailsContainer"><div id="addTempImg">+</div></div><input type="file" id="uploadImage" multiple="multiple" class="hidden"></div><hr><h3>Specifinė info</h3><div id="specificInsertForm"> <?php
                        require("specific/test.html");
                    ?> </div><hr><h3>Kontaktinė info</h3><div id="contactsInsertForm"><label>El. paštas: <input placeholder="email" type="text" class="form-control"></label><label>Telefono nr.: <input placeholder="telefonas" type="text" class="form-control"></label></div><input type="submit"></form></div></div></div><script>var insert =
    {
        maxImages: 0,
        imagesToUpload: [],

        initEvents: function()
        {
            var insertOpenerBtn = $("#insertOpenerBtn");
            insertOpenerBtn.on("click", insert.open);
            insertOpenerBtn.on("click", insert.get);
            $("#insertContainer").on("click", helper.hideDiv);
            $("#nativeSizeImageContainer").on("click", helper.hideDiv);

            var tempThumbnailContainer = $("#tempThumbnailsContainer");
            tempThumbnailContainer.on("click", "> #addTempImg", insert.promptImageUpload);
            tempThumbnailContainer.on("click", "> .tempThumb", insert.thumbnailToNativeSize);
            tempThumbnailContainer.on("click", "> .tempThumb > span", insert.removeThumbnail);
            $("#uploadImage").on("change", function()
            {
                insert.handleImageSelection(this);
            });
        },

        removeThumbnail: function(e)
        {
            e.stopPropagation();

            var removeIndex = $(this).parent().index();
            insert.imagesToUpload.splice(removeIndex, 1);
            $(this).parent().remove();

            $("#addTempImg").show();
        },

        thumbnailToNativeSize: function()
        {
            var nativeSizeContainer = $("#nativeSizeImageContainer");
            nativeSizeContainer.find("> img").attr("src", $(this).find("> img").attr("src"));

            nativeSizeContainer.css("display", "flex");
        },

        handleImageSelection: function(input)
        {
            var addImgBtn = $("#addTempImg");
            var selectedFiles = input.files;

            if (selectedFiles && selectedFiles[0])
            {
                $(selectedFiles).each(function ()
                {
                    if(insert.imagesToUpload.length < insert.maxImages)
                    {
                        insert.imagesToUpload.push(this);
                        var reader = new FileReader();
                        reader.readAsDataURL(this);
                        reader.onload = function(e)
                        {
                            $("<div class='tempThumb'>" +
                                "<img class='thumb' src='" + e.target.result + "'>" +
                                "<span class='glyphicon glyphicon-remove'></span>" +
                                "</div>").insertBefore(addImgBtn);
                        };

                        if(insert.imagesToUpload.length === insert.maxImages) addImgBtn.hide();
                    }
                });
            }

            input.value = null;
        },

        promptImageUpload: function()
        {
            $("#uploadImage").trigger("click");
        },

        open: function()
        {
            if($.isEmptyObject(maps.insertMap)) insert.initMap();

            $("#insertContainer").show();
        },

	    initMap: function()
	    {
		    var insertLocation = maps.searchMarker.getPosition();

		    maps.insertMap = new google.maps.Map(document.getElementById('insertLocationMap'), {
			    zoom: <?php echo $conf['maps']['default']['normalZoom'] ?>,
			    disableDefaultUI: true
		    });

		    maps.insertMarker = new google.maps.Marker({
			    position: insertLocation,
			    map: maps.insertMap
		    });

		    //LOAD GOOGLE MAPS
		    google.maps.event.addListenerOnce(maps.insertMap, 'idle', function()
		    {
			    google.maps.event.trigger(maps.insertMap, 'resize');
			    maps.insertMap.setCenter(insertLocation);
		    });
	    }
    };</script>