<?php
    include_once(__DIR__ . "/includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);

    function generateLanguagesTableHeader()
    {
        global $conf;

        foreach ($conf['languages'] as $language)
        {
            echo "<th>".$language."</td>";
        }
    }

    function getLangTranslates($dbTableName, $columnAppendix = "")
    {
        global $conf;

        $queryLangs = implode($columnAppendix.", ",$conf['languages']);
        $queryLangs .= $columnAppendix." ";

        $query = "
            SELECT id, ".$queryLangs."
            FROM ".$dbTableName."
            ORDER BY id ASC
        ";

        $stmt = $GLOBALS['pdo']->prepare($query);

        $stmt->execute();
        $foundRows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $allRows = "";

        foreach ($foundRows as $key => $row)
        {
            $isId = true;
            $allRows .= "<tr>";

            if($row['en'.$columnAppendix] == "") continue;

            foreach ($row as $key2 => $translation)
            {
                if($isId == false)
                {
                    $allRows .= "<td><input data-target-id='".$row['id']."' data-db-table='".$dbTableName."' data-target-col='".$key2."' class='form-control' value='".$translation."' /></td>";
                }
                $isId = false;
            }

            $allRows .= "</tr>";
        }

        return $allRows;
    }

    function getDBNavigation()
    {
        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT id, layer, parentId, enTitle, position
            FROM AdminNavigation
            ORDER BY
                layer ASC,
                parentId ASC,
                position ASC
        ");

        $stmt->execute();

        $navigation = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return json_encode($navigation);
    }

    function generateLists()
    {
        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT id, enElement
            FROM AdminListsElems
            WHERE parentId = 0
            ORDER BY enElement ASC
        ");

        $stmt->execute();

        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            echo '<a class="list-group-item" data-target-append="#listElems" data-prev-list="#listsDiv" data-list-id="'.$row["id"].'"><span>'.$row["enElement"].'</span><i class="glyphicon glyphicon-edit pull-right"></i></a>';
        }
    }

    function generateAllLists()
    {
        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT id, enElement
            FROM AdminListsElems
            WHERE parentId = 0
            ORDER BY enElement ASC
        ");

        $stmt->execute();

        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            echo '<option value="'.$row["id"].'">'.$row['enElement'].'</option>';
        }
    }
?> <!DOCTYPE html><html><head><meta charset="utf-8"><title>Skelbimų valdymas</title><meta name="viewport" content="width=device-width,initial-scale=1"><link rel="stylesheet" type="text/css" href="css/main.css"><link rel="stylesheet" type="text/css" href="css/valdymas.css"><script>var globals =
            {
                navigationJSON: JSON.parse(JSON.stringify(<?php echo getDBNavigation(); ?>))
            };</script><script src="valdymas.js"></script></head><body><div class="container-fluid"><div class="row"><div class="col-xs-5 col-sm-4 col-md-4 col-lg-3 noPadding" id="navigationContainer"><div id="navigationList" class="border-box" data-nav-id="0"></div></div><div class="col-xs-7 col-sm-8 col-md-8 col-lg-9 noPadding"><header><h2></h2></header><div id="mainContentContainer"><h3>Lauko įkėlimas</h3><label for="insertFormSelect">Tipas:</label><select id="insertFormSelect" class="form-control"><option value="#insertCheckboxFieldForm">Langelis</option><option value="#insertListFieldForm">Sąrašas</option><option value="#insertDoubleListFieldForm">Dvigubas sąrašas</option><option value="#insertNumberFieldForm">Skaičius</option><option value="#insertDateFieldForm">Data</option></select><form id="insertCheckboxFieldForm"><table class="table"><thead><tr><th>Pavadinimas</th><th>Svarbumas</th><th></th></tr></thead><tbody><tr><td><input type="text" name=":enTitle" class="form-control" placeholder="Pavadinimas..."></td><td><select name=":important" class="form-control"><option value="0">Nesvarbus</option><option value="1">Svarbus</option></select></td><td><input type="submit" value="Įkelti" class="btn btn-success"></td></tr></tbody></table></form><form id="insertListFieldForm"><table class="table"><thead><tr><th>DB Tipas</th><th>Pavadinimas</th><th>Sąrašas</th><th>Svarbumas</th><th></th></tr></thead><tbody><tr><td><select name=":dbColType" class="form-control"><option value="TINYINT" title="Iki 255">Tiny Int</option><option value="SMALLINT" title="Iki 65,535">Small Int</option><option value="MEDIUMINT" title="Iki 16,777,215">Medium Int</option></select></td><td><input type="text" name=":enList1Title" class="form-control" placeholder="Pavadinimas..."></td><td><select class="form-control" name="listId" title="Sąrašo pasirinkimas"> <?php
                                                    generateAllLists();
                                                ?> </select></td><td><select name=":important" class="form-control"><option value="0">Nesvarbus</option><option value="1">Svarbus</option></select></td><td><input type="submit" value="Įkelti" class="btn btn-success"></td></tr></tbody></table></form><form id="insertDoubleListFieldForm"><table class="table"><thead><tr><th>DB Tipas</th><th>Pavadinimas 1</th><th>Pavadinimas 2</th><th>Sąrašas</th><th>Svarbumas</th><th></th></tr></thead><tbody><tr><td><select name=":dbColType" class="form-control"><option value="TINYINT" title="Iki 255">Tiny Int</option><option value="SMALLINT" title="Iki 65,535">Small Int</option><option value="MEDIUMINT" title="Iki 16,777,215">Medium Int</option></select></td><td><input type="text" class="form-control" name=":enList1Title" placeholder="Pirmo sąrašo pavadinimas..."></td><td><input type="text" class="form-control" name=":enList2Title" placeholder="Antro sąrašo pavadinimas..."></td><td><select class="form-control" name=":listId" title="Sąrašo pasirinkimas"> <?php
                                                    generateAllLists();
                                                ?> </select></td><td><select name=":important" class="form-control"><option value="0">Nesvarbus</option><option value="1">Svarbus</option></select></td><td><input type="submit" value="Įkelti" class="btn btn-success"></td></tr></tbody></table></form><form id="insertNumberFieldForm"><table class="table"><thead><tr><th>DB Tipas</th><th>Pavadinimas</th><th>VIENGUBAS sąrašas</th><th>Mažiausia reikšmė</th><th>Didžiausia reikšmė</th><th>Svarbumas</th><th></th></tr></thead><tbody><tr><td><select name=":dbColType" class="form-control"><option value="TINYINT" title="Iki 255">Tiny Int</option><option value="SMALLINT" title="Iki 65,535">Small Int</option><option value="MEDIUMINT" title="Iki 16,777,215">Medium Int</option></select></td><td><input type="text" class="form-control" name=":enTitle" placeholder="Skaičiaus pavadinimas"></td><td><select class="form-control" name=":listId" title="Sąrašo pasirinkimas"> <?php
                                                    generateAllLists();
                                                ?> </select></td><td><input type="number" class="form-control" name=":minVal"></td><td><input type="number" class="form-control" name=":maxVal"></td><td><select name=":important" class="form-control"><option value="0">Nesvarbus</option><option value="1">Svarbus</option></select></td><td><input type="submit" value="Įkelti" class="btn btn-success"></td></tr></tbody></table></form><form id="insertDateFieldForm"><table class="table"><thead><tr><th>Pavadinimas</th><th>Svarbumas</th><th></th></tr></thead><tbody><tr><td><input type="text" name=":enList1Title" class="form-control" placeholder="Pavadinimas..."></td><td><select name=":important" class="form-control"><option value="0">Nesvarbus</option><option value="1">Svarbus</option></select></td><td><input type="submit" value="Įkelti" class="btn btn-success"></td></tr></tbody></table></form><table class="table table-hover" id="filterFieldsTable"><thead><tr><th>Tipas</th><th>Sąrašas</th><th>Pavadinimas</th><th>1 sar. pav.</th><th>2 sar. pav.</th><th>Svarbumas</th><th>Duomenų tipas</th><th>Min</th><th>Max</th><th>Operacija</th></tr></thead><tbody></tbody></table><template id="filterFieldRow"><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></template></div></div></div></div><footer class="container-fluid"><div class="row"><div class="col-xs-5 col-sm-4 col-md-4 col-lg-3 noPadding" id="navigationFooter"><div class="input-group"><span class="input-group-btn"><input type="checkbox" title="Ar sukurti nuorodą gilyn?" id="isNewLinkDeep"> </span><input type="text" class="form-control" id="newLinkTitle" placeholder="Nuorodos pavadinimas..."> <span class="input-group-btn"><button id="insertNavLink" class="btn btn-success" type="button">Įkelti</button></span></div></div><div class="col-xs-7 col-sm-8 col-md-8 col-lg-9" id="contentFooter"><button id="listsBtn" class="btn btn-info">Sąrašai</button> <button id="languagesBtn" class="btn btn-primary">Vertimai</button><div class="pull-right"><button id="convertAdminDataBtn" class="btn btn-danger">VERSIJOS PAKEITIMAS</button></div></div></div></footer><div id="languagesContainer"><div class="container"><div class="row"><div class="col-xs-12" id="languagesDiv"><label>Atidaryti išvertimo lentelę:<select class="form-control" id="translationCategorySelect"><option data-target-table="AdminPageElements" data-column-appendix="">Pagrindiniai svetainės elementai</option><option data-target-table="AdminNavigation" data-column-appendix="Title">Svetainės navigacija</option><option data-target-table="AdminListsElems" data-column-appendix="Element">Sąrašų elementai</option><option disabled="disabled">-------LAUKAI-------</option><option data-target-table="AdminTableElements" data-column-appendix="Title">Laukai</option><option data-target-table="AdminTableElements" data-column-appendix="List1Title">1 lygio sąrašai</option><option data-target-table="AdminTableElements" data-column-appendix="List2Title">2 lygio sąrašai</option></select></label><table class="table"><thead><tr> <?php
                                        generateLanguagesTableHeader();
                                    ?> </tr></thead><tbody id="websiteTranslateTable"> <?php
                                    echo getLangTranslates("AdminPageElements");
                                ?> </tbody></table></div></div></div></div><div id="listsContainer"><div class="container"><div class="row"><div class="col-xs-4" id="listsDiv"><br><form id="insertListForm" data-parent-id="0"><div class="input-group width100"><input type="text" class="form-control" placeholder="Sąrašo pavadinimas..."> <input type="submit" class="hidden"></div></form><br><div class="list-group"> <?php
                                generateLists();
                            ?> </div></div><div class="col-xs-4" id="listElems"><br><form id="insertListElementForm" data-parent-id="primary"><div class="input-group width100"><input type="text" class="form-control" placeholder="Elemento pavadinimas..."> <input type="submit" class="hidden"></div></form><br><div class="list-group"></div></div><div class="col-xs-4" id="doubleListElems"><br><form id="insertDoubleListElementForm" data-parent-id="secondary"><div class="input-group width100"><input type="text" class="form-control" placeholder="Elemento pavadinimas..."> <input type="submit" class="hidden"></div></form><br><div class="list-group"></div></div></div></div></div><div id="loaderMain"></div></body></html>