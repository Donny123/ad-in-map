<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    echo insertAdminTableElement();

    function insertAdminTableElement()
    {
        $query = "
            INSERT INTO AdminTableElements(tableName, type, listId, enTitle,enList1Title, enList2Title, important, dbColType, minVal, maxVal)
            VALUES(:tableName, :type, :listId, :enTitle, :enList1Title, :enList2Title, :important, :dbColType, :minVal, :maxVal)
        ";

        $params = isset($_POST['params']) ? $_POST['params'] : '';

        $stmt = $GLOBALS['pdo']->prepare($query);
        $stmt->execute($params);

        return $GLOBALS['pdo']->lastInsertId();
    }
?>