<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    echo insertListElement();

    function insertListElement()
    {
        $query = "
            INSERT INTO AdminListsElems(parentId, enElement)
            VALUES(:parentId, :enElement)
        ";

        $params = isset($_POST['params']) ? $_POST['params'] : '';

        $stmt = $GLOBALS['pdo']->prepare($query);
        $stmt->execute($params);

        return $GLOBALS['pdo']->lastInsertId();
    }
?>