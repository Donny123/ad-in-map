<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    echo json_encode(insertAdminNavigation());

    function insertAdminNavigation()
    {
        $query = "
            INSERT INTO AdminNavigation(layer, parentId, enTitle, position)
            VALUES(:layer, :parentId, :enTitle, :position)";

        $params = isset($_POST['params']) ? $_POST['params'] : '';

        $stmt = $GLOBALS['pdo']->prepare($query);
        $stmt->execute($params);

        return $GLOBALS['pdo']->lastInsertId();
    }
?>