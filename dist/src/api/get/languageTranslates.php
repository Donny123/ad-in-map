<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);

    echo getLangTranslates();

    function getLangTranslates()
    {
        global $conf;

        $dbTableName = $_POST["dbTableName"];
        $columnAppendix = $_POST["columnAppendix"];

        $queryLangs = implode($columnAppendix.", ",$conf['languages']);
        $queryLangs .= $columnAppendix." ";

        $query = "
            SELECT id, ".$queryLangs."
            FROM ".$dbTableName."
            ORDER BY id ASC
        ";

        $stmt = $GLOBALS['pdo']->prepare($query);

        $stmt->execute();
        $foundRows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $allRows = "";

        foreach ($foundRows as $key => $row)
        {
            $isId = true;
            $allRows .= "<tr>";

            if($row['en'.$columnAppendix] == "") continue;

            foreach ($row as $key2 => $translation)
            {
                if($isId == false)
                {
                    $allRows .= "<td><input data-target-id='".$row['id']."' data-db-table='".$dbTableName."' data-target-col='".$key2."' class='form-control' value='".$translation."' /></td>";
                }
                $isId = false;
            }

            $allRows .= "</tr>";
        }

        return $allRows;
    }
?>