<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    updateDb();

    function updateDb()
    {
        $table = isset($_POST['table']) ? $_POST['table'] : '';
        $set = isset($_POST['set']) ? $_POST['set'] : [];
        $where = isset($_POST['where']) ? $_POST['where'] : [];

        //BUILDINU QUERY
        $query =  "UPDATE ".$table." ";
        $query .= "SET ";
        foreach ($set as $key => $val)
        {
            $query .= $key." = '".$val."' AND ";
        }
        $query = substr($query, 0, -5);
        $query .= " WHERE ";
        foreach ($where as $key => $val)
        {
            $query .= $key." = '".$val."' AND ";
        }
        $query = substr($query, 0, -5);

        $stmt = $GLOBALS['pdo']->prepare($query);

        $stmt->execute();
    }
?>