<?php
    include_once(__DIR__ . "/../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    runQuery();

    function runQuery()
    {
        $query = isset($_POST['query']) ? $_POST['query'] : '';
        $params = isset($_POST['params']) ? $_POST['params'] : '';

        $stmt = $GLOBALS['pdo']->prepare($query);
        $stmt->execute($params);
    }
?>
