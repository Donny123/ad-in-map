<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    echo json_encode(getJSON());

    function getJSON()
    {
        $table = isset($_POST['table']) ? $_POST['table'] : '';
        $select = isset($_POST['select']) ? $_POST['select'] : [];
        $where = isset($_POST['where']) ? $_POST['where'] : [];

        //BUILDINU QUERY
        $query =  "SELECT ";
        foreach ($select as $key => $val)
        {
            $query .= htmlentities($val).", ";
        }
        $query = substr($query, 0, -2);
        $query .= " FROM ".$table;
        $query .= " WHERE ";
        foreach ($where as $key => $val)
        {
            $query .= htmlentities($key)." = '".htmlentities($val)."' AND ";
        }
        $query = substr($query, 0, -5);

        $stmt = $GLOBALS['pdo']->prepare($query);

        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return $result;
    }
?>
