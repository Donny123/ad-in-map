<?php
    include_once(__DIR__ . "/../../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);
    echo json_encode(getJSON());

    function getJSON()
    {
        $where = $_POST['where'];

        $query =  "
            SELECT
                ate.id,
                ate.type,
                IfNull(ale.enElement, '') AS enElement,
                ate.enTitle, ate.important,
                ate.enList1Title,
                ate.enList2Title,
                ate.dbColType,
                ate.minVal,
                ate.maxVal
            FROM AdminTableElements ate
            LEFT JOIN AdminListsElems  ale
            ON ate.listId = ale.id
            WHERE ate.tableName = :where
        ";

        $stmt = $GLOBALS['pdo']->prepare($query);

        $stmt->execute([":where" => $where]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return $result;
    }
?>
