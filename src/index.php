<?php
    include_once(__DIR__ . "/includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, false);

    class Client
    {
        public $ip;
        public $countryCode;
        public $lat;
        public $lon;
        public $zoom;

        public $cookiesAgreementVisibility;

        function __construct()
        {
            $this->getIp();
            $this->checkForLocationCookies();
        }

        function setCountryCode($newCountryCode)
        {
            global $conf;

            if((ctype_alpha($newCountryCode)) && (strlen($newCountryCode) == $conf['maps']["countryCode"]["length"]))
            {
                $this->countryCode = strtolower($newCountryCode);
            }
        }

        function setLat($newLat)
        {
            global $conf;

            if(($newLat > $conf['maps']["lat"]["min"]) && ($newLat < $conf['maps']["lat"]["max"]))
            {
                $this->lat = $newLat;
            }
        }

        function setLon($newLon)
        {
            global $conf;

            if(($newLon > $conf['maps']["lon"]["min"]) && ($newLon < $conf['maps']["lon"]["max"]))
            {
                $this->lon = $newLon;
            }
        }

        function locate()
        {
            global $conf;

            if($this->ip != "UNKNOWN")
            {
                $userLocation = json_decode(file_get_contents("http://geoip.nekudo.com/api/".$this->ip));

                if((property_exists($userLocation, "type")) && ($userLocation->type == "error"))
                {
                    $this->countryCode = "en";
                    $this->lat = $conf['maps']['default']['errorLat'];
                    $this->lon = $conf['maps']['default']['errorLon'];
                    $this->zoom = $conf['maps']['default']['errorZoom'];
                }
                else
                {
                    $tenYears = time() + (10 * 365 * 24 * 60 * 60);

                    if(!isset($this->countryCode))
                    {
                        $newCountryCode = strtolower($userLocation->country->code);
                        $this->setCountryCode($newCountryCode);
                        if(isset($_COOKIE['cookieAgreement'])) setCookie("countryCode", $newCountryCode, $tenYears);
                    }
                    if(!isset($this->lat))
                    {
                        $newLatitude = $userLocation->location->latitude;
                        $this->setLat($newLatitude);
                        if(isset($_COOKIE['cookieAgreement'])) setCookie("lat", $newLatitude, $tenYears);
                    }
                    if(!isset($this->lon))
                    {
                        $newLongitude = $userLocation->location->longitude;
                        $this->setLon($newLongitude);
                        if(isset($_COOKIE['cookieAgreement'])) setCookie("lon", $newLongitude, $tenYears);
                    }

                    $this->zoom = $conf['maps']['default']['normalZoom'];
                }
            }
        }

        function getIp()
        {
            if (isset($_SERVER['HTTP_CLIENT_IP'])) $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED'])) $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED'])) $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR'])) $ipaddress = $_SERVER['REMOTE_ADDR'];
            else $ipaddress = 'UNKNOWN';

            $this->ip = $ipaddress;
        }

        function checkForLocationCookies()
        {
            global $conf;

            if(isset($_COOKIE['cookieAgreement']))
            {
                $this->cookiesAgreementVisibility = "none";

                if((isset($_COOKIE['lat'])) && (isset($_COOKIE['countryCode'])) && (isset($_COOKIE['lon'])))
                {
                    $this->setLat($_COOKIE['lat']);
                    $this->setLon($_COOKIE['lon']);
                    $this->setCountryCode($_COOKIE['countryCode']);
                    $this->zoom = $conf['maps']['default']['normalZoom'];
                }
                else $this->locate();
            }
            else
            {
                $this->cookiesAgreementVisibility = "flex";
                $this->locate();
            }
        }
    }

    $client = new Client();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Ad in Map</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrj3gh4dYE5tH7g3uN1qzYNUECMpnG_a4"></script>
        <script>
            var globals =
            {
                client:
                {
                    lat: <?php echo $client->lat; ?>,
                    lng: <?php echo $client->lon; ?>,
                    zoom: <?php echo $client->zoom; ?>,
                    defaultZoom: <?php echo $client->zoom; ?>
                }
            };
        </script>
        <link rel="stylesheet" href="css/main.css">
    </head>

    <body>
        <?php
            include("templates/home/us.php");
        ?>
        <div id="nativeSizeImageContainer">
            <img src="" />
        </div>
        <script type="text/javascript" src="main.js"></script>
    </body>
</html>
