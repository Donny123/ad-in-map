<?php
    include_once(__DIR__ . "/../includes/initialize.php");
    $initialize = new Initialize();
    $initialize->init(true, true);

//    generateIndexFiles();

//    $navigation = getAdminNavigationLinksFull();
//    generateNavigationJSON($navigation);
//
//    $tableNames = getDBTableNames();
//    generateForms($tableNames);

    createNavDbTables();
    createNavDbTableCols();

    function getAdminNavigationLinksFull()
    {
        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT *
            FROM AdminNavigation
            ORDER BY
                layer ASC,
                parentId ASC,
                position ASC
        ");

        $stmt->execute();

        $navigation = $stmt->fetchAll(PDO::FETCH_OBJ);

        return assignNavigationLayers( $navigation );
    }

    function getDBTableNames()
    {
        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT DISTINCT tableName
            FROM AdminNavigation
        ");

        $stmt->execute();

        $tableNames = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $tableNames;
    }

    function generateForms($tableNames)
    {
        foreach ($tableNames as $key => $row)
        {
            $insertHTML = "";
            $filterHTML = "";

            $stmt = $GLOBALS['pdo']->prepare
            ("
                SELECT *
                FROM AdminTableElements
                WHERE tableName = :tableName
            ");

            $stmt->execute([":tableName" => $row['tableName']]);

            $fields = $stmt->fetchAll(PDO::FETCH_OBJ);

            foreach($fields as $i => $field)
            {
                switch ($field->type)
                {
                    case "Langelis":
//                        $insertHTML .= "<label><input type='checkbox' /> ".$field->title."</label>";
                        break;

                    case "Sąrašas":
                        $stmt = $GLOBALS['pdo']->prepare
                        ("
                            SELECT id, element
                            FROM AdminListsElems
                            WHERE parentId = :parentId
                        ");

                        $stmt->execute([":parentId" => $field->listId]);

                        $listElems = $stmt->fetchAll(PDO::FETCH_OBJ);

//                        $insertHTML .= "<label>".$field->list1Title;
                        $insertHTML .= "<select>";
                        foreach($listElems as $k => $listElem)
                        {
                            $insertHTML .= "<option value='".$listElem->id."'>".$listElem->element."</option>";
                        }
                        $insertHTML .= "</select>";
//                        $insertHTML .= "</label>";
                        break;

                    case "Dvigubas sąrašas":
                        $insertHTML .= "D";
                        break;
                }
            }

            $formFile = fopen("../templates/insert/".$row['tableName'].".html", "w");
            fwrite($formFile, $insertHTML);
        }
    }

    function generateIndexFiles()
    {
        global $conf;

        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT *
            FROM AdminPageElements
        ");
        $stmt->execute();
        $foundRows = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($conf['languages'] as $language)
        {
            $fileData = '';

            $fileData .= '<div id="map"></div>';
            $fileData .= '<div id="container">';
            $fileData .=    '<div id="cookieAgreementContainer" class="clickable" style="display:<?php echo $client->cookiesAgreementVisibility ?>">';
            $fileData .=        '<span>This website uses cookies to ensure you get the best experience on our website. <a>Learn more</a></span>';
            $fileData .=        '<div class="centerToRelativeParent" id="cookieAgreementBtn">Got it!</div>';
            $fileData .=    '</div>';
            $fileData .=    '<div class="container-fluid">';
            $fileData .=        '<div class="row">';
            $fileData .=            '<div class="col-lg-3 col-md-4 col-sm-4 clickable" id="resultsContainer">';
            $fileData .=                $foundRows[0]->$language;
            $fileData .=            '</div>';
            $fileData .=            '<div class="col-lg-7 col-md-5 col-sm-5">';
            $fileData .=                '<header class="clickable">';
            $fileData .=                    '<a id="insertOpenerBtn" data-target-id="#insertContainer">Įkelti skelbimą</a><a>Nuoroda 2</a><a>Nuoroda 3</a>';
            $fileData .=                '</header>';
            $fileData .=                '<div class="topicSelectionBtn clickable">';
            $fileData .=                    'Transportas -> Vandens transportas -> valtys';
            $fileData .=                '</div>';
            $fileData .=            '</div>';
            $fileData .=            '<div class="col-lg-2 col-md-3 col-sm-3 clickable" id="filterContainer">';
            $fileData .=                '<form id="filterForm"></form>';
            $fileData .=            '</div>';
            $fileData .=        '</div>';
            $fileData .=    '</div>';
            $fileData .= '</div>';
            $fileData .= '<div id="topicSelectionContainer">';
            $fileData .=    '<div>';
            $fileData .=        '<ul id="topicSelection">';
            $fileData .=            '<li>0</li>';
            $fileData .=            '<li>1</li>';
            $fileData .=        '</ul>';
            $fileData .=        '<ul id="subtopicSelection1"></ul>';
            $fileData .=        '<ul id="subtopicSelection2"></ul>';
            $fileData .=    '</div>';
            $fileData .= '</div>';
            $fileData .= '<div id="insertContainer">';
            $fileData .=    '<div class="container">';
            $fileData .=        '<div class="row">';
            $fileData .=            '<div class="col-sm-12">';
            $fileData .=                '<div class="topicSelectionBtn">';
            $fileData .=                    'tests->asdfa';
            $fileData .=                '</div>';
            $fileData .=                '<form id="insertForm">';
            $fileData .=                    '<h3>Bendra info</h3>';
            $fileData .=                    '<div id="generalInsertForm">';
            $fileData .=                        '<label>Antraštė: <input placeholder="Antraštė" type="text" class="form-control"></label>';
            $fileData .=                        '<label>Nuotraukos t.t. <input type="file" class="form-control" /></label>';
            $fileData .=                    '</div>';
            $fileData .=                    '<hr />';
            $fileData .=                    '<h3>Specifinė info</h3>';
            $fileData .=                    '<div id="specificInsertForm">';
            $fileData .=                        '';
            $fileData .=                    '</div>';
            $fileData .=                    '<hr />';
            $fileData .=                    '<h3>Kontaktinė info</h3>';
            $fileData .=                    '<div id="contactsInsertForm">';
            $fileData .=                        '<label>El. paštas: <input placeholder="email" type="text" class="form-control"></label>';
            $fileData .=                        '<label>Telefono nr.: <input placeholder="telefonas" type="text" class="form-control"></label>';
            $fileData .=                    '</div>';
            $fileData .=                '</form>';
            $fileData .=            '</div>';
            $fileData .=        '</div>';
            $fileData .=    '</div>';
            $fileData .= '</div>';
            $fileData .= '<script>';
            $fileData .=    'var navLinks =';
            $fileData .=    '{';
            $fileData .=        '0:{';
            $fileData .=            't: "0",';
            $fileData .=            'e: "test",';
            $fileData .=            '0:';
            $fileData .=            '{';
            $fileData .=                't:"0-0",';
            $fileData .=                'e:"link name"';
            $fileData .=            '},';
            $fileData .=            '1:';
            $fileData .=            '{';
            $fileData .=                't:"0-1",';
            $fileData .=                'e:"link name2",';
            $fileData .=                '0:';
            $fileData .=                '{';
            $fileData .=                    't:"0-1-0",';
            $fileData .=                    'e:"link name23"';
            $fileData .=                '}';
            $fileData .=            '}';
            $fileData .=        '},';
            $fileData .=        '1:';
            $fileData .=        '{';
            $fileData .=            't: "0",';
            $fileData .=            'e: "test22"';
            $fileData .=        '}';
            $fileData .=    '}';
            $fileData .= '</script>';

            $newFile = fopen("../templates/home/".$language.".php", "w");
            fwrite($newFile, $fileData);
        }
    }

    function createNavDbTables()
    {
        $stmt = $GLOBALS['pdo']->prepare
        ("
            SELECT id
            FROM AdminNavigation
            WHERE isTableCreated = 0
        ");
        $stmt->execute();
        $foundRows = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($foundRows as $notCreatedTableRow)
        {
            $stmt = $GLOBALS['pdo']->prepare
            ("
                CREATE TABLE IF NOT EXISTS Items_".$notCreatedTableRow->id."
                (
                    id int NOT NULL AUTO_INCREMENT,
                    PRIMARY KEY (id)
                ) ENGINE=MYISAM AUTO_INCREMENT=0 CHARSET=utf8;
            ");
            $stmt->execute();
        }

        $stmt = $GLOBALS['pdo']->prepare
        ("
            UPDATE AdminNavigation
            SET isTableCreated = 1
            WHERE isTableCreated = 0
        ");
        $stmt->execute();
    }

    function createNavDbTableCols()
    {
//        $stmt = $GLOBALS['pdo']->prepare
//        ("
//            SELECT id, important, dbColType, tableName
//            FROM AdminTableElements
//            WHERE needAlter = 1
//        ");
//        $stmt->execute();
//        $foundFields = $stmt->fetchAll(PDO::FETCH_OBJ);
//
//        foreach ($foundFields as $field)
//        {
//            $stmt = $GLOBALS['pdo']->prepare
//            ("
//                ALTER TABLE Items_".$field->tableName."
//                    ADD column_".$field->id." ".$field->dbColType."
//            ");
//            $stmt->execute();
//
//            if($field->important == 1)
//            {
//                $stmt = $GLOBALS['pdo']->prepare
//                ("
//                    ALTER TABLE Items_".$field->tableName."
//                        ADD INDEX (column_".$field->id.")
//                ");
//                $stmt->execute();
//            }
//        }
//
//        $stmt = $GLOBALS['pdo']->prepare
//        ("
//            UPDATE AdminTableElements
//            SET needAlter = 0
//            WHERE needAlter = 1
//        ");
//        $stmt->execute();
    }

    function assignNavigationLayers( $navigation )
   {
       $rootNavigation = [];

       //LAYER 1 SUKURIMAS
       foreach ($navigation as $key => $navLink)
       {
           if($navLink->parentId == 0)
           {
//               $rootNavigation[$navLink->title] = $navLink;

               unset($navigation[$key]);
           }
       }

       //LAYER 2 SUKURIMAS
       foreach ($rootNavigation as $key1 => $navLink1)
       {
           $targetId = $navLink1->id;

           foreach ($navigation as $key => $navLink)
           {
               if($navLink->parentId == $targetId)
               {
//                   $navLink1->{$navLink->title} = $navLink;

                   unset($navigation[$key]);
               }
           }
       }

       //LAYER 3 SUKURIMAS
       foreach ($rootNavigation as $key1 => $navLink1)
       {
           foreach ($navLink1 as $key2 => $navLink2)
           {
               if(isset($navLink2->id))
               {
                   $targetId = $navLink2->id;

                   foreach ($navigation as $key => $navLink)
                   {
                       if($navLink->parentId == $targetId)
                       {
//                           $navLink2->{$navLink->title} = $navLink;

                           unset($navigation[$key]);
                       }
                   }
               }
           }
       }

       //REMOVE EMPTY CHILDREN
       foreach ($rootNavigation as $key1 => $navLink1)
       {
           if(is_object($navLink1))
           {
               foreach ($navLink1 as $key2 => $navLink2)
               {
                   if(is_object($navLink2))
                   {
                       foreach ($navLink2 as $key3 => $navLink3)
                       {
                           if(is_object($navLink3))
                           {
                               unsetNavigationValues($navLink3);
                           }
                       }
                       unsetNavigationValues($navLink2);
                   }
               }
               unsetNavigationValues($navLink1);
           }
       }

       return $rootNavigation;
   }

    function unsetNavigationValues($object)
    {
        $object->t = $object->tableName;
        unset($object->tableName);

        unset($object->id);
        unset($object->parentId);
        unset($object->layer);
        unset($object->position);
//        unset($object->title);
    }

    function generateNavigationJSON($nav)
    {
        $navJSON = fopen('../navigation.json', 'w');
        fwrite($navJSON, json_encode($nav));
        fclose($navJSON);
    }
?>
