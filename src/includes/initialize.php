<?php
	session_start();

	include_once(__DIR__.'/../config.php');

	class Initialize
	{
		public function init($shouldConnectToDb, $isAdminPage)
		{
			ob_start();
			$this->checkIfMaintenance();
			$this->showErrors();
			if($shouldConnectToDb)
			{
				$this->connectToDb();
			}
			if($isAdminPage)
			{
				$this->checkIfIsAdmin();
			}
			$this->initGoogleAnalytics();
		}

		private function connectToDb()
		{
			$GLOBALS['pdo'] = new PDO("mysql:host=".$GLOBALS["conf"]['database']['host'].";dbname=".$GLOBALS["conf"]['database']['name'].";charset=utf8mb4", $GLOBALS["conf"]['database']['username'], $GLOBALS["conf"]['database']['password']) or die(mysql_error());
		}

		private function checkIfIsAdmin()
		{
			$isAdmin = isset($_SESSION['admin']) ? $_SESSION['admin'] : false;

			if($isAdmin != true)
			{
				exit();
			}
		}

		private function showErrors()
		{
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
		}

		private function checkIfMaintenance()
		{
			if($GLOBALS['conf']['inMaintenance'] == "true")
			{
				$IPWhitelist = ["85.232.148.24", "78.61.8.5", "127.0.0.1"];

				if(!in_array($this->getIp(), $IPWhitelist))
				{
					include_once(__DIR__.'/../maintenance.php');
					exit();
				}
			}
		}

		private function getIp()
		{
			if(isset($_SERVER['HTTP_CLIENT_IP']))
			{
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			}
			else
			{
				if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				{
					$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
				else
				{
					if(isset($_SERVER['HTTP_X_FORWARDED']))
					{
						$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
					}
					else
					{
						if(isset($_SERVER['HTTP_FORWARDED_FOR']))
						{
							$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
						}
						else
						{
							if(isset($_SERVER['HTTP_FORWARDED']))
							{
								$ipaddress = $_SERVER['HTTP_FORWARDED'];
							}
							else
							{
								if(isset($_SERVER['REMOTE_ADDR']))
								{
									$ipaddress = $_SERVER['REMOTE_ADDR'];
								}
								else
								{
									$ipaddress = 'UNKNOWN';
								}
							}
						}
					}
				}
			}

			return $ipaddress;
		}

		private function initGoogleAnalytics()
		{
			echo "
				<script>
		            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-102583580-1', 'auto');ga('send', 'pageview');
		        </script>
			";
		}
	}
?>
