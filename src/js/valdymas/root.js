import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import "../../css/valdymas.scss";

String.prototype.toHtml = function()
{
    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };

    return String(this).replace(/[&<>"'\/]/g, function (s)
    {
        return entityMap[s];
    });
};

var core =
{
    sendAjax: function(dataType, method, targetPage, data, useLoader, putContentTo, ajaxCallback)
    {
        $.ajax({
            dataType: dataType,
            method: method,
            url: targetPage,
            data: data,
            beforeSend: function()
            {
                if(useLoader == true) $("#loaderMain").show();

            }
        }).done(function(data)
        {
            if(putContentTo != "")
            {
                $(putContentTo).html(data);
            }
            ajaxCallback(data);
            if(useLoader == true) $("#loaderMain").hide();
        }).fail(function(xhr, status, error)
        {
            console.log("ERRORAS!");
            console.log(error);
            alert("Kažkas netaip, bandyk per nauja arba manes klausk ;)");
            if(useLoader == true) $("#loaderMain").fadeOut();
        });
    },

    init: function()
    {
        console.log(globals.navigationJSON);
        var navStructure = globals.navigationJSON;

        for(var i in navStructure)
        {
            if (navStructure.hasOwnProperty(i))
            {
                var currentNavLink = navStructure[i];

                var navLinkTemplate = "" +
                    "<div class='navigationLink border-box' data-nav-id='"+currentNavLink.id+"' data-position='"+currentNavLink.position+"' data-table-name='"+currentNavLink.id+"'>" +
                    "   <span>"+currentNavLink.enTitle+"</span>" +
                    "   <i class='glyphicon glyphicon-resize-vertical moveNavLink' title='Perkelti nuorodą'></i>" +
                    "   <i class='glyphicon glyphicon-edit editNavLink' title='Pakeisti nuorodą'></i>" +
                    "   <i class='glyphicon glyphicon-remove removeNavLink' title='Pašalinti nuorodą'></i>" +
                    "</div>" +
                    "<div class='expandableNavList'>" +
                    "" +
                    "</div>";

                if(currentNavLink.layer == 1)
                {
                    $("#navigationList").append(navLinkTemplate)
                }
                else
                {
                    $("#navigationList").find("div[data-nav-id='"+currentNavLink.parentId+"']").next(".expandableNavList").append(navLinkTemplate)
                }
            }
        }

        core.initEvents();
    },

    initEvents: function()
    {
        navigation.initEvents();
        lists.initEvents();
        fields.initEvents();
        converter.initEvents();
        languages.initEvents();
    }
};

var languages =
{
    initEvents: function()
    {
        $("#languagesBtn").on("click", languages.showLanguagesDiv);
        $("#languagesContainer").on("click", languages.hideLanguagesDiv);
        $("#languagesDiv").find("> table > tbody > tr > td > input").on("blur", languages.updateTranslation);
        $("#translationCategorySelect").on("change", languages.getLanguageTranslations);
    },

    getLanguageTranslations: function()
    {
        var targetTable = $(this).find(":selected").data("target-table");
        var columnAppendix = $(this).find(":selected").data("column-appendix");

        var ajaxData = {
            dbTableName: targetTable,
            columnAppendix: columnAppendix
        };

        core.sendAjax("HTML", "POST", "api/get/languageTranslates.php", ajaxData, true, "#websiteTranslateTable", function(){});
    },

    showLanguagesDiv: function()
    {
        $("#languagesContainer").show();
    },

    hideLanguagesDiv: function(e)
    {
        e = window.event || e;

        if(this === e.target)
        {
            $("#languagesContainer").hide();
        }
    },

    updateTranslation: function()
    {
        var ajaxData =
        {
            "table": $(this).data("db-table"),
            "set": {},
            "where": {
                "id": $(this).data("target-id")
            }
        };

        ajaxData.set[$(this).data("target-col")] = $(this).val();

        core.sendAjax("HTML", "POST", "api/update/masterAdmin.php", ajaxData, false, "", function(){});
    }
};

var fields =
{
    initEvents: function()
    {
        $("#insertFormSelect").on("change", fields.openInsertFieldForm);
        $("#mainContentContainer > form").on("submit", fields.insertField);
        $("#filterFieldsTable").find("> tbody").on("click", ".removeFieldBtn", fields.removeField);
    },

    openInsertFieldForm: function()
    {
        $("#mainContentContainer").find("> form").hide();
        $($(this).val()).show();
    },

    insertField: function()
    {
        var tableName = $(".selectedNavLink").data("table-name");
        var fieldType = $("#insertFormSelect").find("> :selected").text();
        if(typeof tableName == "undefined")
        {
            alert("Nepasirinkta navigacijos nuoroda");
            return false;
        }

        var formData = $(this).serializeArray();

        var ajaxData =
        {
            "params": {
                ":tableName": tableName,
                ":type": fieldType,
                ":listId": "0",
                ":enTitle": "",
                ":enList1Title": "",
                ":enList2Title": "",
                ":important": "0",
                ":dbColType": "MEDIUMINT"
            }
        };

        for(var i in formData)
        {
            if (formData.hasOwnProperty(i))
            {
                var currentData = formData[i];
                ajaxData.params[currentData.name] = currentData.value;
            }
        }

        core.sendAjax("HTML", "POST", "api/insert/AdminTableElements.php", ajaxData, true, "", function()
        {
            $(".selectedNavLink").trigger("click");
        });

        return false;
    },

    removeField: function()
    {
        var fieldId = $(this).data("filter-id");
        var currentRow = $(this).parent().parent();

        if(window.confirm("Ar tikrai nori ištrinti pasirinktą lauką?"))
        {
            var ajaxData =
            {
                "query": "" +
                    "DELETE FROM AdminTableElements " +
                    "WHERE id = :id",
                "params": {
                    ":id": fieldId
                }
            };

            core.sendAjax("HTML", "POST", "api/adminQuery.php", ajaxData, true, "", function()
            {
                currentRow.remove();
            });
        }
        return false;
    }
};

var lists =
{
    initEvents: function()
    {
        $("#listsBtn").on("click", lists.showLists);
        $("#listsContainer").on("click", lists.hideLists);
        $("#listsDiv").find("> div").on("click", "> a", lists.openList);
        $("#listElems").find("> div").on("click", "> a", lists.openList);
        $("#insertListForm").on("submit", lists.insertList);
        $("#insertListElementForm").on("submit", lists.insertList);
        $("#insertDoubleListElementForm").on("submit", lists.insertList);
        $("#listsDiv").find("> div").on("click", "> a > i", lists.editElement);
        $("#listElems").find("> div").on("click", "> a > i", lists.editElement);
        $("#doubleListElems").find("> div").on("click", "> a > i", lists.editElement);
    },

    insertList: function()
    {
        var listName;
        var parentId;
        var listType = $(this).data("parent-id");

        if(listType == "primary")
        {
            listName = $("#insertListElementForm").find("> div > input").val();
            parentId = $("#listsDiv").find("> div > .active").data("list-id");
        }
        else if(listType == "secondary")
        {
            listName = $("#insertDoubleListElementForm").find("> div > input").val();
            parentId = $("#listElems").find("> div > .active").data("list-id");
        }
        else if(listType == 0)
        {
            listName = $("#insertListForm").find("> div > input").val();
            parentId = 0;
        }

        var ajaxData =
        {
            "params": {
                ":parentId": parentId,
                ":enElement": listName
            }
        };

        core.sendAjax("HTML", "POST", "api/insert/AdminListsElems.php", ajaxData, true, "", function( insertedId )
        {
            if(listType == "primary") $("#listElems").find("> div").prepend("<a class='list-group-item' data-target-append='#doubleListElems' data-prev-list='#listElems' data-list-id='"+insertedId+"'><span>"+listName+"</span><i class='glyphicon glyphicon-edit pull-right'></i></a>");
            else if(listType == "secondary") $("#doubleListElems").find("> div").prepend("<a class='list-group-item' data-target-append='#doubleListElems' data-prev-list='#listElems' data-list-id='"+insertedId+"'><span>"+listName+"</span><i class='glyphicon glyphicon-edit pull-right'></i></a>");
            else if(listType == 0) $("#listsDiv").find("> div").prepend("<a class='list-group-item' data-target-append='#listElems' data-prev-list='#listsDiv' data-list-id='"+insertedId+"'><span>"+listName+"</span><i class='glyphicon glyphicon-edit pull-right'></i></a>");


            $("#insertListElementForm").find("> div > input").val("");
            $("#insertDoubleListElementForm").find("> div > input").val("");
            $("#insertListForm").find("> div > input").val("");
        });

        return false;
    },

    openList: function()
    {
        var listId = $(this).data("list-id");
        var appendTarget = $(this).data("target-append");
        var prevListId = $(this).data("prev-list");

        $(prevListId).find("> div > .active").removeClass("active");
        $(this).addClass("active");

        var ajaxData =
        {
            "table": "AdminListsElems",
            "select": {
                "0": "id",
                "1": "enElement"
            },
            "where": {
                "parentId": listId
            }
        };

        core.sendAjax("JSON", "POST", "api/get/masterAdmin.php", ajaxData, true, "", function( listElems )
        {
            listElems.sort(function(a, b)
            {
                if (a.enElement < b.enElement) return -1;
                if (a.enElement > b.enElement) return 1;
                return 0;
            });
            var listElemsDiv = $(appendTarget);

            $("#doubleListElems").hide().find("> div").empty();
            listElemsDiv.show().find("> div").empty();

            for(var i in listElems)
            {
                if(listElems.hasOwnProperty(i))
                {
                    var row = listElems[i];

                    listElemsDiv.find("> div").append('<a class="list-group-item" data-prev-list="#listElems" data-target-append="#doubleListElems" data-list-id="'+row["id"]+'"><span>'+row["enElement"]+'</span><i class="glyphicon glyphicon-edit pull-right"></i></a>');
                }
            }
        });
    },

    showLists: function()
    {
        $("#listsContainer").show();
    },

    hideLists: function(e)
    {
        e = window.event || e;

        if(this === e.target)
        {
            $("#listsContainer").hide();
        }
    },

    editElement: function()
    {
        var listElementElement = $(this).prev("span");
        var listElementId = $(this).parent().data("list-id");
        var listElementTitle = listElementElement.text();

        var newListName = prompt("Naujas sąrašo/sąrašo elemento pavadinimas: ", listElementTitle);

        if((newListName != null) && (newListName != ""))
        {
            var ajaxData =
            {
                "table": "AdminListsElems",
                "set": {
                    "enElement": newListName.toHtml()
                },
                "where": {
                    "id": listElementId
                }
            };

            core.sendAjax("HTML", "POST", "api/update/masterAdmin.php", ajaxData, true, "", function()
            {
                listElementElement.text(newListName);
            });
        }
        return false;
    }
};

var navigation =
{
    initEvents: function()
    {
        var navigationListElem = $("#navigationList");

        navigationListElem.on("click", ".navigationLink", navigation.getLinkContent);
        navigationListElem.on("click", ".navigationLink > input", navigation.changeVisibilityState);
        navigationListElem.on("click", ".moveNavLink", navigation.selectMoveableLink);
        navigationListElem.on("click", ".editNavLink", navigation.editLinkName);
        navigationListElem.on("click", ".removeNavLink", navigation.removeLink);

        $("#insertNavLink").on("click", navigation.insertLink);
    },

    getLinkContent: function()
    {
        var targetNav = $(this).data("table-name");
        $(".selectedNavLink").removeClass("selectedNavLink");
        $(this).addClass("selectedNavLink");

        $("header > h2").text($(this).find("> span").text());

        $(this).next(".expandableNavList").stop(true, false).slideToggle();

        var ajaxData =
        {
            "where": targetNav
        };

        core.sendAjax("JSON", "POST", "api/get/AdminTableElements.php", ajaxData, true, "", function( filterFields )
        {
            var filterFieldTemplate = $("#filterFieldRow");
            var insertFilterTemplate = $("#insertFilterRow");
            var filterFieldsTableBody = $("#filterFieldsTable").find("> tbody");

            filterFieldsTableBody.empty().append(insertFilterTemplate.html());
            for(var i in filterFields)
            {
                if(filterFields.hasOwnProperty(i))
                {
                    var row = filterFields[i];

                    filterFieldsTableBody.append(filterFieldTemplate.html())
                        .find("> tr:last-child > td:nth-child(1)")
                        .text(row.type)
                        .next().text(row.enElement)
                        .next().text(row.enTitle)
                        .next().text(row.enList1Title)
                        .next().text(row.enList2Title)
                        .next().text(row.important)
                        .next().text(row.dbColType)
                        .next().text(row.minVal)
                        .next().text(row.maxVal)
                        .next().html("<input type='button' class='btn btn-danger removeFieldBtn' value='Pašalinti' data-filter-id='"+row.id+"' />");
                }
            }
        });
    },

    changeVisibilityState: function()
    {
        $(this).parent().toggleClass("hiddenNavLink visibleNavLink");

        var targetVisibilityState;
        if(this.checked) targetVisibilityState = 1;
        else targetVisibilityState = 0;

        var navLinkId = $(this).parent().data("nav-id");

        var ajaxData =
        {
            "table": "AdminNavigation",
            "set": {
                "isVisible": targetVisibilityState
            },
            "where": {
                "id": navLinkId
            }
        };

        core.sendAjax("HTML", "POST", "api/update/masterAdmin.php", ajaxData, true, "", function(){});

        return true;
    },

    selectMoveableLink: function()
    {
        $(".selectedMoveLink").removeClass("selectedMoveLink");
        $(this).parent().addClass("selectedMoveLink");

        $("#navigationList").off("click", ".navigationLink").on("click", ".navigationLink", navigation.attemptMove);
        return false;
    },

    attemptMove: function()
    {
        var link1 = $(".selectedMoveLink");
        var link2 = $(this);

        if(link1.is(link2))
        {
            link1.removeClass("selectedMoveLink");
            $("#navigationList").off("click", ".navigationLink").on("click", ".navigationLink", navigation.getLinkContent);
        }
        else if(link1.siblings().is(link2))
        {
            var link1Id = $(link1).data("nav-id");
            var link2Id = $(link2).data("nav-id");
            var link1Pos = $(link1).data("position");
            var link2Pos = $(link2).data("position");
            var link1Expandables = $(link1).next(".expandableNavList");
            var link2Expandables = $(link2).next(".expandableNavList");

            link1.removeClass("selectedMoveLink");
            $("#navigationList").off("click", ".navigationLink").on("click", ".navigationLink", navigation.getLinkContent);

            $(link1).data("position", link2Pos);
            $(link2).data("position", link1Pos);

            var ajaxData =
            {
                "table": "AdminNavigation",
                "set": {
                    "position": link2Pos
                },
                "where": {
                    "id": link1Id
                }
            };
            core.sendAjax("HTML", "POST", "api/update/masterAdmin.php", ajaxData, true, "", function()
            {
                ajaxData =
                {
                    "table": "AdminNavigation",
                    "set": {
                        "position": link1Pos
                    },
                    "where": {
                        "id": link2Id
                    }
                };

                core.sendAjax("HTML", "POST", "api/update/masterAdmin.php", ajaxData, true, "", function(){
                    navigation.swapLinks(link1, link2, link1Expandables, link2Expandables);
                });
            });
        }
        else
        {
            alert("Negalima keistis vietom su šiuo elementu");
        }
    },

    swapLinks: function(link1, link2, link1Expandable, link2Expandable)
    {
        var link1Clone = link1.clone(true);
        var link2Clone = link2.clone(true);
        var link1ExpandableClone = link1Expandable.clone(true);
        var link2ExpandableClone = link2Expandable.clone(true);

        link1.replaceWith(link2Clone);
        link2.replaceWith(link1Clone);
        link1Expandable.replaceWith(link2ExpandableClone);
        link2Expandable.replaceWith(link1ExpandableClone);
    },

    editLinkName: function()
    {
        var linkId = $(this).parent().data("nav-id");
        var linkNameElement = $(this).siblings("span");
        var currentLinkName = linkNameElement.text();
        var newLinkName = prompt("Naujas nuorodos pavadinimas: ", currentLinkName);

        if((newLinkName != null) && (newLinkName != ""))
        {
            var ajaxData =
            {
                "table": "AdminNavigation",
                "set": {
                    "enTitle": newLinkName.toHtml()
                },
                "where": {
                    "id": linkId
                }
            };

            core.sendAjax("HTML", "POST", "api/update/masterAdmin.php", ajaxData, true, "", function()
            {
                linkNameElement.text(newLinkName);
            });
        }
        return false;
    },

    removeLink: function()
    {
        var linkElement = $(this).parent();
        var linkParentId = (linkElement.parent().is(".expandableNavList") ? linkElement.parent().prev(".navigationLink").data("nav-id") : 0);
        var linkPosition = (linkElement.index()/2) + 1;
        var linkId = linkElement.data("nav-id");
        var linkElementTitle = linkElement.find("> span").text();

        if(window.confirm("Ar tikrai nori ištrinti "+linkElementTitle+" IR giliau esančias nuorodas?"))
        {
            var ajaxData =
            {
                "query": "" +
                    "UPDATE AdminNavigation " +
                    "SET position = position - 1 " +
                    "WHERE position > :rootPosition " +
                    "AND parentId = :rootParentId ",
                "params": {
                    ":rootPosition": linkPosition,
                    ":rootParentId": linkParentId
                }
            };

            core.sendAjax("HTML", "POST", "api/adminQuery.php", ajaxData, true, "", function()
            {
                var ajaxData =
                {
                    "query": "" +
                        "DELETE FROM AdminNavigation " +
                        "WHERE id IN ( " +
                            "SELECT id FROM( " +
                                "SELECT id " +
                                "FROM AdminNavigation " +
                                "WHERE id = :rootId " +
                                "" +
                                "OR " +
                                "" +
                                "parentId IN( " +
                                "SELECT id " +
                                "FROM AdminNavigation " +
                                "WHERE id = :rootId " +
                            ") " +
                            "" +
                            "OR " +
                            "" +
                            "parentId IN( " +
                                "SELECT id " +
                                "FROM AdminNavigation " +
                                "WHERE parentId IN( " +
                                    "SELECT id " +
                                    "FROM AdminNavigation " +
                                    "WHERE id = :rootId " +
                                ") " +
                            ") " +
                        " ) AS helper " +
                    ")",
                    "params": {
                        ":rootId": linkId
                    }
                };

                core.sendAjax("HTML", "POST", "api/adminQuery.php", ajaxData, true, "", function()
                {
                    linkElement.next().remove();
                    linkElement.remove();
                });
            });
        }
        return false;
    },

    insertLink: function()
    {
        var selectedLink = $(".selectedNavLink");
        var linkTitleInput = $("#newLinkTitle");

        if(selectedLink.length == 1)
        {
            var isDeep = ($("#isNewLinkDeep").is(":checked") == true);
            var linkTitle = linkTitleInput.val();
            var linkLayer;
            var navigationListElement = $("#navigationList");
            var linkPosition;
            var parentId;

            linkTitleInput.val("");
            if(isDeep)
            {
                parentId = selectedLink.data("nav-id");
            }
            else if(selectedLink.parent().attr("id") != "navigationList")
            {
                parentId = selectedLink.parent().prev().data("nav-id");
            }
            else
            {
                parentId = 0;
            }

            if(isDeep)
            {
                if(navigationListElement.find("> .selectedNavLink").length > 0) linkLayer = 2;
                else if(navigationListElement.find("> .expandableNavList > .selectedNavLink").length > 0) linkLayer = 3;
                else if(navigationListElement.find("> .expandableNavList > .expandableNavList > .selectedNavLink").length > 0)
                {
                    alert("Per giliai bandai sukurt nuorodą");
                    return;
                }

                linkPosition = (selectedLink.next(".expandableNavList").children().length / 2) + 1
            }
            else
            {
                if(navigationListElement.find("> .selectedNavLink").length > 0) linkLayer = 1;
                else if(navigationListElement.find("> .expandableNavList > .selectedNavLink").length > 0) linkLayer = 2;
                else if(navigationListElement.find("> .expandableNavList > .expandableNavList > .selectedNavLink").length > 0) linkLayer = 3;

                linkPosition = (selectedLink.parent().children().length / 2) + 1;
            }

            var removeParentDb = false;
            if((isDeep == true) && (linkPosition == 1)) removeParentDb = true;

            var ajaxData =
            {
                "params": {
                    ":layer": linkLayer,
                    ":parentId": parentId,
                    ":enTitle": linkTitle.toHtml(),
                    ":position": linkPosition
                },
                removeParentDb: removeParentDb
            };

            core.sendAjax("JSON", "POST", "api/insert/AdminNavigation.php", ajaxData, true, "", function( insertId )
            {
                var navLinkTemplate = "" +
                    "<div class='navigationLink border-box hiddenNavLink' data-is-full-ad='0' data-nav-id='"+insertId+"' data-position='"+linkPosition+"' data-table-name='"+insertId+"'>" +
                    "   <span>"+linkTitle+"</span>" +
                    "   <input type='checkbox' title='Pakeisti matomumą' />" +
                    "   <i class='glyphicon glyphicon-resize-vertical moveNavLink' title='Perkelti nuorodą'></i>" +
                    "   <i class='glyphicon glyphicon-edit editNavLink' title='Pakeisti nuorodą'></i>" +
                    "   <i class='glyphicon glyphicon-remove removeNavLink' title='Pašalinti nuorodą'></i>" +
                    "</div>" +
                    "<div class='expandableNavList'>" +
                    "" +
                    "</div>";
                if(isDeep)
                {
                    $(".selectedNavLink").next(".expandableNavList").append(navLinkTemplate);
                }
                else
                {
                    $(".selectedNavLink").parent().append(navLinkTemplate);
                }
            });
        }
        else alert("Nepasirinkai nuorodos prie/kurioje bus įkelta nauja nuoroda");
    }
};

var converter =
{
    initEvents: function()
    {
        $("#convertAdminDataBtn").on("click", converter.convert);
    },

    convert: function()
    {
        if (confirm('Ar tikrai nori konvertuoti esamus failus?'))
        {
            core.sendAjax("HTML", "JSON", "includes/convertAdminFiles.php", {}, true, "", function()
            {
                alert("PAKEISTA VERSIJA");
            })
        }
    }
};

$(document).ready(core.init);
