import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import "../../css/index.scss";

function requireAll(r) { r.keys().forEach(r); }

requireAll(require.context('../../', true, /\.php$/));
requireAll(require.context('../../', true, /\.html$/));

window.maps = {
    mainMap: {},
    searchMarker: {},

    insertMap: {},
    insertMarker: {},

    initEvents: function()
    {

    },

    initMap: function()
    {
        var searchLocation =
        {
            lat: globals.client.lat,
            lng: globals.client.lng
        };

        maps.mainMap = new google.maps.Map(document.getElementById('map'), {
            zoom: globals.client.zoom,
            center: searchLocation,
            disableDefaultUI: true
        });

        maps.searchMarker = new google.maps.Marker({
            position: searchLocation,
            map: maps.mainMap
        });
    }
};

window.config =
{
    maps:
    {
        normalZoom: 10,
        errorZoom: 2,
        errorLat: 45,
        errorLon: -18,
        countryCodeLength: 2,
        latMin: -90,
        latMax: 90,
        lonMin: -180,
        lonMax: 180
    }
};

window.main =
{
    language: "",

    sendAjax: function(dataType, method, targetPage, data, useLoader, putContentTo, ajaxCallback)
    {
        $.ajax({
            dataType: dataType,
            method: method,
            url: targetPage,
            data: data,
            beforeSend: function()
            {
            //    if(useLoader == true) $("#loaderMain").show();
            }
        }).done(function(data)
        {
            if(putContentTo != "")
            {
                $(putContentTo).html(data);
            }
            ajaxCallback(data);
        //    if(useLoader == true) $("#loaderMain").hide();
        }).fail(function()
        {
        //    if(useLoader == true) $("#loaderMain").fadeOut();
        });
    },

    init: function()
    {
        main.initEvents();
        maps.initMap();
    },

    initEvents: function()
    {
        maps.initEvents();
        cookies.initEvents();
        sections.initEvents();
        filters.initEvents();
        insert.initEvents();
        navigation.initEvents();
    }
};

window.helper =
{
    hideDiv: function(e)
    {
        e = window.event || e;

        if(this === e.target)
        {
            $("#"+this.id).hide();
        }
    },

    openDiv: function()
    {
        var targetId = $(this).data("target-id");

        $(targetId).show();
    }
};

window.filters =
{
    initEvents: function()
    {

    },

    get: function()
    {
        main.sendAjax("HTML", "GET", "templates/filters/us/test.html", {}, false, "#filterContainer", function(){});
    }
};

window.navigation =
{
    initEvents: function()
    {

    }
};

window.insert =
{
    maxImages: 0,
    imagesToUpload: [],

    get: function()
    {
        console.log("S");
        main.sendAjax("HTML", "GET", "templates/insert/us.php", {}, false, "#specificInsertForm", function(){});
    },

    initEvents: function()
    {
        console.log("G");
        var insertOpenerBtn = $("#insertOpenerBtn");
        insertOpenerBtn.on("click", insert.open);
        insertOpenerBtn.on("click", insert.get);
        $("#insertContainer").on("click", helper.hideDiv);
        $("#nativeSizeImageContainer").on("click", helper.hideDiv);

        var tempThumbnailContainer = $("#tempThumbnailsContainer");
        tempThumbnailContainer.on("click", "> #addTempImg", insert.promptImageUpload);
        tempThumbnailContainer.on("click", "> .tempThumb", insert.thumbnailToNativeSize);
        tempThumbnailContainer.on("click", "> .tempThumb > span", insert.removeThumbnail);
        $("#uploadImage").on("change", function()
        {
            insert.handleImageSelection(this);
        });
    },

    removeThumbnail: function(e)
    {
        e.stopPropagation();

        var removeIndex = $(this).parent().index();
        insert.imagesToUpload.splice(removeIndex, 1);
        $(this).parent().remove();

        $("#addTempImg").show();
    },

    thumbnailToNativeSize: function()
    {
        var nativeSizeContainer = $("#nativeSizeImageContainer");
        nativeSizeContainer.find("> img").attr("src", $(this).find("> img").attr("src"));

        nativeSizeContainer.css("display", "flex");
    },

    handleImageSelection: function(input)
    {
        var addImgBtn = $("#addTempImg");
        var selectedFiles = input.files;

        if (selectedFiles && selectedFiles[0])
        {
            $(selectedFiles).each(function ()
            {
                if(insert.imagesToUpload.length < insert.maxImages)
                {
                    insert.imagesToUpload.push(this);
                    var reader = new FileReader();
                    reader.readAsDataURL(this);
                    reader.onload = function(e)
                    {
                        $("<div class='tempThumb'>" +
                            "<img class='thumb' src='" + e.target.result + "'>" +
                            "<span class='glyphicon glyphicon-remove'></span>" +
                            "</div>").insertBefore(addImgBtn);
                    };

                    if(insert.imagesToUpload.length === insert.maxImages) addImgBtn.hide();
                }
            });
        }

        input.value = null;
    },

    promptImageUpload: function()
    {
        $("#uploadImage").trigger("click");
    },

    open: function()
    {
        console.log("C");
        if($.isEmptyObject(maps.insertMap)) insert.initMap();

        $("#insertContainer").show();
    },

    initMap: function()
    {
        var insertLocation = maps.searchMarker.getPosition();

        maps.insertMap = new google.maps.Map(document.getElementById('insertLocationMap'), {
            zoom: globals.client.defaultZoom,
            disableDefaultUI: true
        });

        maps.insertMarker = new google.maps.Marker({
            position: insertLocation,
            map: maps.insertMap
        });

        //LOAD GOOGLE MAPS
        google.maps.event.addListenerOnce(maps.insertMap, 'idle', function()
        {
            google.maps.event.trigger(maps.insertMap, 'resize');
            maps.insertMap.setCenter(insertLocation);
        });
    }
};

window.sections =
{
    canClose: false,

    selectedTopic: "",
    selectedSubtopic1: "",
    selectedSubtopic2: "",

    initEvents: function()
    {
        var topicSelectionContainer = $("#topicSelectionContainer");
        topicSelectionContainer.on("click", sections.close);
        topicSelectionContainer.find("> div").on("click", function(){event.stopPropagation()});
        $("#topicSelection, #subtopicSelection1, #subtopicSelection2").on("click", "> li", sections.selectCategory);
        $(".topicSelectionBtn").on("click", sections.open);
    },

    open: function()
    {
        $("#topicSelectionContainer").css("display", "flex");
    },

    close: function()
    {
        if(sections.canClose === true)
        {
            $("#topicSelectionContainer").hide();
        }
    },

    selectCategory: function()
    {
        var subtopicSelectionList1 = $("#subtopicSelection1");
        var subtopicSelectionList2 = $("#subtopicSelection2");
        var selectedNum = $(this).index();
        var layer = $(this).parent().index() + 1;
        var containerToUse;

        var listToCreate;

        if(layer === 1)
        {
            subtopicSelectionList1.empty();
            subtopicSelectionList2.empty();

            listToCreate = navLinks[selectedNum];
            containerToUse = subtopicSelectionList1;
            sections.selectedTopic = selectedNum;
        }
        else if(layer === 2)
        {
            subtopicSelectionList2.empty();

            listToCreate = navLinks[sections.selectedTopic][selectedNum];
            containerToUse = subtopicSelectionList2;
            sections.selectedSubtopic1 = selectedNum;
        }
        else
        {
            listToCreate = navLinks[sections.selectedTopic][sections.selectedSubtopic1][selectedNum];

            sections.selectedSubtopic2 = selectedNum;
        }

        var hasLinks = false;
        var currentIndex = 0;
        while(listToCreate.hasOwnProperty(currentIndex.toString()))
        {
            hasLinks = true;
            containerToUse.append("<li>"+listToCreate[currentIndex].e+"</li>");
            currentIndex++;
        }

        if(!hasLinks)
        {
            var sectionsPath = navLinks[sections.selectedTopic].e;
            if(layer === 2)sectionsPath += " -> "+navLinks[sections.selectedTopic][sections.selectedSubtopic1].e;
            if(layer === 3)sectionsPath += " -> "+navLinks[sections.selectedTopic][sections.selectedSubtopic1].e+" -> "+navLinks[sections.selectedTopic][sections.selectedSubtopic1][sections.selectedSubtopic2].e;
            $(".topicSelectionBtn").html(sectionsPath);

            filters.get();
            if($("#insertContainer").is(":visible")) insert.get();

            sections.canClose = true;
            sections.close();
        }
    }
};

window.cookies =
{
    initEvents: function()
    {
        $("#cookieAgreementBtn").on("click", cookies.agreePolicy);
    },

    agreePolicy: function()
    {
        $("#cookieAgreementContainer").fadeOut();
        cookies.set("cookieAgreement", "1");
    },

    get: function(cname)
    {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++)
        {
            var c = ca[i];
            while (c.charAt(0) == ' ')
            {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0)
            {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },

    set: function(cname, cvalue)
    {
        var CookieDate = new Date;
        CookieDate.setFullYear(CookieDate.getFullYear( ) +10);
        document.cookie = cname + "=" + cvalue + ";expires="+CookieDate.toGMTString()+";path=/";
    }
};

$(document).ready(main.init);
