<?php
    include_once(__DIR__ . "/includes/initialize.php");

    $initialize = new Initialize();
    $initialize->init(true, false);
    initPage();

    function initPage()
    {
        $isAdmin = isset($_SESSION['admin']) ? $_SESSION['admin'] : false;

        if($isAdmin == true) redirectToAdminPanel();
        checkGetCode();
        attemptLogin();
    }

    function checkGetCode()
    {
        $code1 = isset($_GET['code1']) ? $_GET['code1'] : '';
        $code2 = isset($_GET['code2']) ? $_GET['code2'] : '';
        $code3 = isset($_GET['code3']) ? $_GET['code3'] : '';

        if(($code1 != $GLOBALS["conf"]["adminLogin"]['code1']) || ($code2 != $GLOBALS["conf"]["adminLogin"]['code2']) || ($code3 != $GLOBALS["conf"]["adminLogin"]['code3']))
        {
            die();
        }
    }

    function attemptLogin()
    {
        $password = isset($_POST['password']) ? $_POST['password'] : '';

        if(($password != "") && ($password == $GLOBALS["conf"]['adminLogin']['password'])) redirectToAdminPanel();
    }

    function redirectToAdminPanel()
    {
        $_SESSION['admin'] = true;

        header("Location: ".$GLOBALS["conf"]['home']."/valdymas.php");
        die();
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Prisijungimas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/prisijungimas.css">
        <script src="prisijungimas.js" ></script>
    </head>
    <body>
        <div id="mainContainer">
            <form id="loginContainer" method="POST" action="" class="form-inline">
                <h1 class="text-center"><span>:D</span> Privet :D</h1>
                <input type="text" name="password" class="form-control" placeholder="Pagrindinis slaptažodis..." />
                <input type="submit" class="btn btn-success" />
            </form>
        </div>
    </body>
</html>
